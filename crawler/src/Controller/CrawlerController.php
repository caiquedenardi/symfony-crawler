<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DomCrawler\Crawler;

class CrawlerController extends AbstractController
{
    /**
     * @Route("/", name="crawler")
     */
    public function index()
    {	
    	//url

    	$url = 'https://animesonlinebr.site/';
    	
    	// vá buscar os dados do URL

		$client = new \GuzzleHttp\Client();
		$response = $client->request('GET', $url);
		$html = ''.$response->getBody(); 

		$crawler = new Crawler($html);


		// percorre os dados

		$items = $crawler->filter('div.telinhas > ul > li')->each(function (Crawler $node, $i) {
			
			// pesquise valores que eu quero
		    

		   	//echo $node->html();
		   	$text = $node->text();
		   	$text = explode(".", $text);
		   	$text = str_replace("Episódios", "", $text);
		   	$image =  $node->filter('img')->attr('data-src');
		   	$link = $node->filter('a')->attr('href');
		   	$item = [
		   		'image' => $image,
		   		'date' => $text[0],
		   		'text' => $text[1],
		   		'link' => $link
		   	];
		   	return $item;

		});

        return $this->render('index.html.twig', ['items'=>$items]);
    }
}
